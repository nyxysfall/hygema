#!/usr/bin/env hy
(import argparse)
(import re)
(import [urllib [urlopen]])

(setv URL_BASE "https://www.urbanomic.com/glossget.php?status=idle&system=AQ&wait=0&glossary=")

(defn cipher [in-str]
  (setv filtered (filter (fn [x]
                           (if (or
                                 (.isdigit x)
                                 (.isalpha x))
                               x))
                         in-str))
  (sum (list (map (fn [x] (cond [(.isalpha x)
                                 (- (int (ord (.lower x))) 87)]
                                [(.isdigit x)
                                 (int x)]
                                ))
                  filtered))))


(defn sreg [in-str]
  (setv matches (re.match r"^(\d+)(=.+)$" in-str))
  (re.sub r"^(=)" "= " (matches.group 2)))

(defn body-parse [body]
  (setv new-body [])
  (for [line body]
    (.append new-body (sreg line)))
  new-body)

(defn url-conv [in-url]
  (.splitlines
    (.decode
      (.read (urlopen in-url))
      "utf-8")))

(defn url-clean [in-url]
  (del (cut in-url 0 2))
  (del (get in-url -1))
  (.join "\n" (body-parse in-url)))

(defn ret-eq [in-url]
  (url-clean (url-conv in-url)))

(defn get-eq [in-str]
  (setv str-val (cipher in-str))
  (setv url (+ URL_BASE
               f"&phrase{in-str}&result1={str-val}"))
  (setv eq-list (ret-eq url))
  (print (+ in-str
            " = "
            (str str-val)
            "\n"
            eq-list)))

 (defn get-rev [in-int]
   (setv url (+ URL_BASE
                f"&result1={(str in-int)}"))
   (setv eq-list (ret-eq url))
   (print (+ (str in-int)
             "\n"
             eq-list)))

(defmain [&rest _]
  (setv parser (argparse.ArgumentParser
                 :description "AQ gematria cipher"))
  (.add-argument parser "-s" "--string"
                 :type str
                 :help "string to cipher")
  (.add-argument parser "-r" "--reverse"
                 :type int
                 :help "reverse search a number")
  (setv args (parser.parse_args))
  (cond
    [(do args.string)
     (get-eq args.string)]
    [(do args.reverse)
     (get-rev args.reverse)]))
